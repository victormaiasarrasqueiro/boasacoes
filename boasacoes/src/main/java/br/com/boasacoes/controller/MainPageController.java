package br.com.boasacoes.controller;

import java.security.Principal;

import javax.inject.Inject;
import javax.inject.Provider;

import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.boasacoes.repository.UserRepository;

@Controller
public class MainPageController {
	
	private final Provider<ConnectionRepository> connectionRepositoryProvider;

	private final UserRepository userRepository;

	@Inject
	public MainPageController(Provider<ConnectionRepository> connectionRepositoryProvider,  UserRepository userRepository) {
		this.connectionRepositoryProvider = connectionRepositoryProvider;
		this.userRepository = userRepository;
	}

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home(Principal currentUser, Model model) {
		model.addAttribute("connectionsToProviders", connectionRepositoryProvider.get().findAllConnections());
		if (currentUser != null) {
			model.addAttribute(userRepository.findUserByUsername(currentUser.getName()));
		}
		return "main";
	}

}

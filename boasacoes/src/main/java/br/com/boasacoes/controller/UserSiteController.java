package br.com.boasacoes.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.boasacoes.repository.UserRepository;

@Controller
@RequestMapping(path="/site/user")
public class UserSiteController {
	
	@Autowired
	private UserRepository userRepository;

	private Facebook facebook;
	private ConnectionRepository connectionRepository;
	
	public UserSiteController(Facebook facebook, ConnectionRepository connectionRepository) {
        this.facebook = facebook;
        this.connectionRepository = connectionRepository;
    }
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String prepare1(Model model) {
		
		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/connect/facebook";
        }

        model.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
        PagedList<Post> feed = facebook.feedOperations().getFeed();
        model.addAttribute("feed", feed);
        
		return "site/user/user_main";
	}
	
	@RequestMapping(value="/profile", method=RequestMethod.GET)
	public String home(Principal currentUser, Model model) {
		model.addAttribute("feed", "VICTOR MAIA SARRASQUEIRO");
		return "user-geral";
	}

}

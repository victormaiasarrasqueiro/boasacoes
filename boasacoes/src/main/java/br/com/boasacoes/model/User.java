package br.com.boasacoes.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 950441018411071216L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private BigInteger id;
	
	@NotNull
	@Size(min=3, max=60)
	@Column(name="username")
	@JsonProperty(value = "username")
	private String username;
	
	@NotNull
	@Size(min=3, max=20)
	@JsonIgnore
	@Column(name="password")
	private String password;
	
	@NotNull
	@Size(min=3, max=60)
	@Column(name="email")
	@JsonProperty(value = "email")
	private String email; 

	@NotNull
	@Size(min=3, max=45)
	@JsonProperty(value = "firstname")
	@Column(name="first_name")
	private String firstName;  
	
	@NotNull
	@Size(min=3, max=60)
	@Column(name="last_name")
	@JsonProperty(value = "lastname")
	private String lastName; 

	@Past
	@JsonProperty(value = "dtB")
	@Column(name="dt_birth")
	private Date dtBirth;  
	
	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@Column(name="genre")
	@JsonProperty(value = "genre")
	private Byte genre;   

	@Size(min=1, max=100)
	@Column(name="facebook_id")
	@JsonIgnore
	private String facebookId; 
	
	@Size(min=1, max=100)
	@Column(name="twitter_id")
	@JsonIgnore
	private String twitterId; 
	
	@Size(min=1, max=100)
	@Column(name="linkedin_id")
	@JsonIgnore
	private String linkedinId; 
	
	@Past
	@JsonIgnore
	@Column(name="dt_reg")
	@JsonProperty(value = "dtReg")
	private Date dtRegistration ;
	
	@JsonIgnore
	@Column(name="active")
	@JsonProperty(value = "active")
	private Boolean active;

	public User(String username, String password, String firstName, String lastName) {
		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = username;
	}
	
	public User(String username, String password, String firstName, String lastName, String email) {
		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public User() {
		super();
	}
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDtBirth() {
		return dtBirth;
	}

	public void setDtBirth(Date dtBirth) {
		this.dtBirth = dtBirth;
	}

	public Byte getGenre() {
		return genre;
	}

	public void setGenre(Byte genre) {
		this.genre = genre;
	}

	public Date getDtRegistration() {
		return dtRegistration;
	}

	public void setDtRegistration(Date dtRegistration) {
		this.dtRegistration = dtRegistration;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the facebookId
	 */
	public String getFacebookId() {
		return facebookId;
	}

	/**
	 * @param facebookId the facebookId to set
	 */
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	/**
	 * @return the twitterId
	 */
	public String getTwitterId() {
		return twitterId;
	}

	/**
	 * @param twitterId the twitterId to set
	 */
	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	/**
	 * @return the linkedinId
	 */
	public String getLinkedinId() {
		return linkedinId;
	}

	/**
	 * @param linkedinId the linkedinId to set
	 */
	public void setLinkedinId(String linkedinId) {
		this.linkedinId = linkedinId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}


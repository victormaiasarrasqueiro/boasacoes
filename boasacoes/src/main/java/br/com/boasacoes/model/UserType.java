package br.com.boasacoes.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@Entity
public class UserType implements Serializable  {

	private static final long serialVersionUID = 3193804691544970930L;
	
	@Id
	@JsonProperty(value = "id")
	private Byte id;
	
	@NotNull
	@Size(min=2, max=10)
	@JsonProperty(value = "nm")
	private String name;

	public Byte getId() {
		return id;
	}

	public void setId(Byte id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}


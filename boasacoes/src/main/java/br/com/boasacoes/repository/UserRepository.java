package br.com.boasacoes.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.boasacoes.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findUserByUsername(String username);
}
